from django.shortcuts import render
from django.http import HttpResponse


def home(request):
    context = {"year": "2023"}
    return render(request, "home/home.html", context)


def handler400(request, *args, **kwargs):
    return render(request, "400.html", status=400)


def handler403(request, *args, **kwargs):
    return render(request, "403.html", status=403)


def handler404(request, *args, **kwargs):
    return render(request, "404.html", status=404)


def handler500(request, *args, **kwargs):
    return render(request, "500.html", status=500)


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")
