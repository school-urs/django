from django.urls import path
from . import views

# см. Пространства имен URL
app_name = "home"

urlpatterns = [
    path("", views.home, name="index")
]
