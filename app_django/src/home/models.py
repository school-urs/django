from django.db import models


class HomeTest(models.Model):
    test_text = models.CharField(max_length=255)
    pub_date = models.DateTimeField("date published")
