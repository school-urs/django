from django.urls import include, path
from . import views

# https://docs.djangoproject.com/en/4.2/topics/http/urls/

example_include_patterns = [
    path("sub-include/", views.view_none),
    path("sub-include/<int:id>/", views.view_none),
    path("sub-include/test/", views.view_none),
]


urlpatterns = [
    path("", views.index, name="example_index"),
    path("example-render/", views.example_render, name="example_render"),
    # Преобразователи путей
    path("str/<str:name>/", views.view_none),
    path("int/<int:name>/", views.view_none),
    path("slug/<slug:name>/", views.view_none),
    path("uuid/<uuid:name>/", views.view_none),
    path("path/<path:name>/", views.view_none),
    # Указание значений по умолчанию для аргументов представления
    path("default/", views.default_page),
    path("default/page<int:num>/", views.default_page),
    # Пример include
    path("include/", include(example_include_patterns)),
    path("include-urls-py/", include("src.home.urls", namespace="home2")),
    # Пример устранения дублирования "<page_slug>-<page_id>/""
    path(
        "<page_slug>-<page_id>/",
        include(
            [
                path("history/", views.view_none),
                path("edit/", views.view_none),
                path("discuss/", views.view_none),
                path("permissions/", views.view_none),
            ]
        ),
    ),
    # Передача дополнительных опций для просмотра функций
    path("props/<int:year>/", views.props, {"foo": "bar"}),
    path("props-v2/<int:year>/", views.props_v2, {"foo": "bar"}),
    # Наименование путей
    path("articles/<int:year>/", views.view_none, name="news-year-archive"),
    # Пространства имен URL
    path("home-use-namespace/", include("src.home.urls", namespace="home3")),
]
