from django.shortcuts import render
from django.http import HttpResponse


def example_render(request):
    context = {"year": "2023"}
    return render(request, "_example/example_render.html", context)


def index(request):
    return HttpResponse("Example Index! Hello, world!")


# Указание значений по умолчанию для аргументов представления
def default_page(request, num=1):
    return HttpResponse(f"Example default_page! Page: { num }")


# Передача дополнительных опций для просмотра функций
def props(request, year, foo):
    return HttpResponse(f"Example props! year: { year } foo: {foo}")


# Передача дополнительных опций для просмотра функций
def props_v2(request, year, **kwargs):
    return HttpResponse(f"Example props v2! year: { year } foo: { kwargs['foo'] }")


# Заглушка для примеров путей
def view_none(request):
    return HttpResponse(None)
