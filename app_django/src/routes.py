from django.conf import settings
from django.urls import path, include


urlpatterns = [
    path("account/", include("src.account.urls")),
    path("example/", include("src._example.urls")),
    path("", include("src.home.urls", namespace="home")),
]
