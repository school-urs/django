# Django App

## Development env

### DEV-LITE

Для тестовых запусков на БД SQLite - первый запуск для проверки

```bash
# Создать виртуальное окружение если не создано (создает в кэше poetry) и активировать
poetry shell

# Установка dev зависимостей (всех кроме групп stage и prod)
poetry install --without stage,prod

# Пример. Добавить зависимости в группу dev
poetry add flake8 mypy black isort --group dev


############################3

[tool.poetry.dev-dependencies]
pytest = "^7.1.2"
pytest-django = "^4.5.2"
django-extensions = "^3.1.5"
ipdb = "^0.13.9"
black = "^22.3.0"
ipython = "^8.4.0"

###########################33

# Запуск проекта
# Необходимо чтоб все значения переменных в env были в кавычках
set -a && source .env.dev && set +a && \
python ./app_django/manage.py runserver --settings=config.settings.dev-lite

set -a && source .env.dev && set +a
python ./app_django/manage.py makemigrations --settings=config.settings.dev-lite
python ./app_django/manage.py migrate --settings=config.settings.dev-lite
python ./app_django/manage.py createsuperuser --settings=config.settings.dev-lite
```

### DEV (DOCKER)

Для для разработки в полноценном окружении

```bash
# Запустить контейнеры
docker compose -f docker-compose.dev.yml --env-file .env.dev up --build -d
# Остановить и удалить все контейнеры и вольюмы
docker stop $(docker ps -qa) && docker rm $(docker ps -qa) && docker volume rm $(docker volume ls -q)
# Удалить неиспользуемые образы
docker system prune
```

Для входа в контейнеры

```bash
docker exec -it django-dev-nginx /bin/sh
docker exec -it django-dev-django /bin/sh
docker exec -it django-dev-postgresql /bin/sh
```

Создать симв. ссылки

```bash
docker exec -it django-dev-nginx /bin/sh
ln -s /etc/nginx/sites-available/localtest.me.conf /etc/nginx/sites-enables/
nginx -t
service nginx restart
```

Входим в контейнер Джанго и создаем суперюзера

```bash
docker exec -it django-dev-django /bin/sh
# python manage.py migrate - применение миграций для DEV вынесено в ентрипоинт
python manage.py createsuperuser
```

PG_ADMIN

Админ панель для PostgreSQL доступна по адресу: <http://localtest.me/pgadmin/>
