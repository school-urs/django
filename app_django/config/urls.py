from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("src.routes")),
]

# Пользовательские обработчики ошибок
# Не забывайте, что DEBUG для этого необходимо установить значение False, иначе будут использоваться обычные обработчики отладки.
handler400 = "src.home.views.handler400"
handler403 = "src.home.views.handler403"
handler404 = "src.home.views.handler404"
handler500 = "src.home.views.handler500"

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
