from .base import *

DEBUG = int(os.environ.get('DEBUG', default=1))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}
