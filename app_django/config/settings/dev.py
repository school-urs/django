from .base import *

DEBUG = int(os.environ.get('DEBUG', default=1))
# DEBUG = False

DATABASES = {
    "default": {
        "ENGINE": str(os.environ.get("POSTGRES_ENGINE", "django.db.backends.postgresql_psycopg2")),
        "NAME": str(os.environ["POSTGRES_DB"]),
        "USER": str(os.environ["POSTGRES_USER"]),
        "PASSWORD": str(os.environ["POSTGRES_PASSWORD"]),
        "HOST": str(os.environ.get("POSTGRES_HOST", "localhost")),
        "PORT": str(os.environ.get("POSTGRES_PORT", "5432")),
    }
}
