# Docker-compose проекта Django/Nginx/Postgresql/Redis/RabbitMQ/Celery

## Назначение

Предназначено для развертывания вэбсервиса на Django.

## DEVELOPMENT

### Клонируем репозиторий

Создаем в домашней директории (или любой другой) каталог для проекта и клонируем репозиторий.

```bash
cd ~/
mkdir django_test
cd ~/django_test
git clone https://gitlab.com/school-urs/django.git .
```

### Запуск контейнеров DEV

#### Настройка

В корне проекта создаем файл .env.dev и копируем в него содержимое .env.example
В переменную окружения "URS_ENV_HOME" присваиваем путь до корня проекта (смотрим командой pwd).
В моем случае:

```bash
URS_ENV_HOME='/home/ural-site/django_test/'
```

Так же создаем каталог для базы данных в соответствии с настройкой "PGDATA_NAME"

```bash
cd ./databases/postgresql/
mkdir dev
cd ../../
```

Далее для того, чтоб избежать глюков с гитом, необходимо каталог для БД и редис добавить в группу докер:

```bash
sudo chgrp -R -v docker ./databases/ && \
sudo chmod -R -v g+rw ./databases/ && \
sudo chgrp -R -v docker ./caches/ && \
sudo chmod -R -v g+rw ./caches/
```

#### Запускаем контейнеры DEV

Для запуска контейнеров воспользуйтесь командой:

```bash
# Запуск контейнеров из каталога проекта
docker compose -f docker-compose.dev.yml --env-file .env.dev up --build -d

# Остановка и удаление контейнеров и повторный запуск и очистка от неиспользуемых
docker stop $(docker ps -qa) && docker rm $(docker ps -qa) && docker volume rm $(docker volume ls -q) && \
docker compose -f docker-compose.dev.yml --env-file .env.dev up --build -d && docker system prune
```

#### Создать симв. ссылки

После того как контейнеры успешно запущены, необходимо сделать символическую ссылку настроек nginx локального домена "localtest.me"
Для того, чтоб данный локальный домен был доступен, нужно внести его в файл hosts у себя на локальной машине:

```bash
sudo nano /etc/hosts
```

В данном файле после "127.0.0.1       localhost" с новой строки добавляем "127.0.0.2       localtest.me"

А далее создаем символическую ссылку на конфиг в контейнере nginx:

```bash
# Входим в контейнер
docker exec -it django-dev-nginx /bin/sh
# Создаем символическую ссылку на конфиг
ln -s /etc/nginx/sites-available/localtest.me.conf /etc/nginx/sites-enables/
# Проверяем настройки, ошибок быть не должно
nginx -t
# Перезапускаем nginx
service nginx restart
```

После этого сайт должен быть досупен по домену <http://localtest.me/>.

#### Администрирование

Интерфейс администрора реализован в стандартной панели администратора Django:

<http://localtest.me/>
<http://localtest.me/admin>
<http://localtest.me/pgadmin>

### Входт в контейнер

```bash
# Django App
docker exec -it django-dev-django /bin/sh
# Nginx
docker exec -it django-dev-nginx /bin/sh
# Postgresql
docker exec -it django-dev-postgresql /bin/sh
```

### Создать дамп БД

```bash
docker exec -it django-dev-postgresql /bin/sh
pg_dump django_dev_db --format=tar -U django_dev_user > /tmp/backups/pg_dump/django_dev_db_$(date '+%Y-%m-%d-%H-%M').tar
```

### Применить миграции

```bash
docker exec -it django-dev-django /bin/sh
python manage.py migrate
```

### Создать приложение

```bash
set -a && source .env.dev && set +a && \
cd ./app_django/src/ && \
python ../manage.py startapp appname && \
cd ../../
```

Но проще не создавать приложение, а копировать приложение "_example" и прописывать в конфиге

### Создать файлы миграций

```bash
source ./app_django/venv/bin/activate && \
set -a && source .env.dev && set +a && \
python ./app_django/manage.py makemigrations --settings=config.settings.dev-lite && \
python ./app_django/manage.py migrate --settings=config.settings.dev-lite
```
